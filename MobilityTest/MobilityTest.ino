#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

#define LMotorSpeed 112
#define RMotorSpeed 100
#define axle_sensor_delay 660 // Used to align the axle with board line after seen by the line sensors 
#define lineSensitivity 650 // 1 - will always see line, 999 - will never see line
#define tokenSensitivity 950
#define intersectionDelay 500 // Time to move forward before line following again
#define WAIT_FOR_START 0
#define LEG_01 1
#define LEG_02 2
#define LEG_03 3
#define LEG_04 4
#define LEG_05 5
#define LEG_06 6
#define LEG_07 7
#define LEG_08 8
#define LEG_09 9
#define LEG_10 10
#define END 10
#define ledPin 22

// Motor Shield initialization
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);

// Line Follower Sensors and variables
int sensorPin1 = A12;
int sensorPin2 = A11;
int sensorPin3 = A10;
int sensorPin4 = A9;
int sensorPin5 = A8;
int sensor1 = 0;  
int sensor2 = 0;
int sensor3 = 0;
int sensor4 = 0;
int sensor5 = 0;
int lineCount = 0;
int position = 0;

// Wheel encoder variables
int encoder0PinA = 2; //interrupt pin
int encoderCount = 0;

// FSM variables
int state, nextState;

void setup() {
  Serial.begin(9600); // set up Serial library at 9600 bps
  Serial.println("IEEE Robot Mobility Test");
  AFMS.begin(); // create with the default frequency 1.6KHz
//  attachInterrupt(digitalPinToInterrupt(2), wheelEncoderISR, CHANGE);
  state = WAIT_FOR_START;
  pinMode(ledPin,OUTPUT);
}

void loop() {
  switch(state){
    case WAIT_FOR_START:
      // Todo: Wait for go button to be pressed.
//      while(1){
//        rotateLeft(2);
//        getPosition();
//        delay(3000);
//        }
      nextState = LEG_01;
      break;
    case LEG_01:
      leftMotor->setSpeed(LMotorSpeed); // Range of 0-255
      rightMotor->setSpeed(RMotorSpeed);
      leftMotor->run(FORWARD);
      rightMotor->run(FORWARD);
      while(getPosition()!=0){} // Until line 1 is found, while no line present
      delay(axle_sensor_delay); // Until line 1 is crossed, while line present
      while(getPosition()!=0){} // Until line 2 is found, while no line present
      delay(axle_sensor_delay);
      rotateLeft(1);
      delay(100);
      nextState = LEG_02;
      break;
      
    case LEG_02:
      followLine();
      if(isToken()==1){ // If grey token found at intersection 
        digitalWrite(ledPin, HIGH);
        // Todo: wait until lift and storage is done 
      }
      else{ // If grey token is not found at the intersection.
        digitalWrite(ledPin, LOW); 
      }
      leftMotor->setSpeed(LMotorSpeed); // Range of 0-255
      rightMotor->setSpeed(RMotorSpeed);
      leftMotor->run(FORWARD);
      rightMotor->run(FORWARD);
      delay(axle_sensor_delay);
      leftMotor->run(RELEASE);
      rightMotor->run(RELEASE);
      rotateRight(2);
      nextState = LEG_03;
      break;
      
    case LEG_03:
      followLine();
      if(isToken()==1){ // If token present
        digitalWrite(ledPin, HIGH);
        // Todo: wait until lift and storage is done
      }
      else{
        digitalWrite(ledPin, LOW);
      }
      nextState = LEG_04;
      break;
      
    case LEG_04:
      leftMotor->run(FORWARD);
      rightMotor->run(FORWARD);
      delay(intersectionDelay);
      followLine();
      if(isToken()==1){ // If token is present
        digitalWrite(ledPin, HIGH);
        // Todo : wait until lift and storage is done
        }
      else{
        digitalWrite(ledPin, LOW);
      }
      nextState = LEG_05;
      break;
      
    case LEG_05:
      leftMotor->run(FORWARD);
      rightMotor->run(FORWARD);
      delay(intersectionDelay);
      followLine();
      if(isToken()==1){ // If token is present
        digitalWrite(ledPin, HIGH);
        // TODO: Wait until lift and storage subsystem is done.
        }
      else{
        digitalWrite(ledPin, LOW); 
      }
      leftMotor->run(FORWARD);
      rightMotor->run(FORWARD);
      delay(axle_sensor_delay);
      rotateLeft(1);
      nextState = LEG_06;
      break;
      
    case LEG_06:
      followLine();
      if(isToken()==1){ // If token present
        digitalWrite(ledPin, HIGH);
      }
      else{
        digitalWrite(ledPin, LOW);
      }
      leftMotor->run(FORWARD);
      rightMotor->run(FORWARD);
      delay(axle_sensor_delay);
      rotateLeft(1);
      nextState = LEG_07;
      break;
      
    case LEG_07:
      followLine();
      if(isToken()==1){ // If token present
        digitalWrite(ledPin, HIGH);
      }
      else{
        digitalWrite(ledPin, LOW);
      }
      nextState = LEG_08;
      break;

    case LEG_08:
      followLine();
      if(isToken()==1){ // If token present
        digitalWrite(ledPin, HIGH);
      }
      else{
        digitalWrite(ledPin, LOW);
      }
      nextState = LEG_09;
      break;

    case LEG_09:
      followLine();
      if(isToken()==1){ // If token present
        digitalWrite(ledPin, HIGH);
      }
      else{
        digitalWrite(ledPin, LOW);
      }
      leftMotor->run(FORWARD);
      rightMotor->run(FORWARD);
      delay(axle_sensor_delay);
      rotateRight(1);
      nextState = END;
      break;
    case END:
      leftMotor -> run(RELEASE);
      rightMotor -> run(RELEASE);
      while(1);
      break;
  }
  state = nextState;
}

void followLine(){
      position = getPosition();
      while(position!=0&&position!=10){ // While not at an intersection and a line exists
        leftMotor->run(FORWARD);
        rightMotor->run(FORWARD);
        switch(position){
          case 1:
            leftMotor->setSpeed(LMotorSpeed-20); // Range of 0-255
            rightMotor->setSpeed(RMotorSpeed+20);
            break;
          case 2:
            leftMotor->setSpeed(LMotorSpeed-15);
            rightMotor->setSpeed(RMotorSpeed+15);
            break;
          case 3:
            leftMotor->setSpeed(LMotorSpeed-10);
            rightMotor->setSpeed(RMotorSpeed+10);
            break;
          case 4:
            leftMotor->setSpeed(LMotorSpeed-5);
            rightMotor->setSpeed(RMotorSpeed+5);
            break;
          case 5:
            leftMotor->setSpeed(LMotorSpeed);
            rightMotor->setSpeed(RMotorSpeed);
            break;
          case 6:
            leftMotor->setSpeed(LMotorSpeed+5);
            rightMotor->setSpeed(RMotorSpeed-5);
            break;
          case 7:
            leftMotor->setSpeed(LMotorSpeed+10);
            rightMotor->setSpeed(RMotorSpeed-10);
            break;
          case 8:
            leftMotor->setSpeed(LMotorSpeed+15);
            rightMotor->setSpeed(RMotorSpeed-15);
            break;
          case 9:
            leftMotor->setSpeed(LMotorSpeed+20);
            rightMotor->setSpeed(RMotorSpeed-20);
            break;
          default:
            leftMotor->setSpeed(LMotorSpeed);
            rightMotor->setSpeed(RMotorSpeed);
            break;
        }
        position = getPosition();
      }
      if(state==LEG_02){
        delay(110);
      }
      rightMotor->run(RELEASE);
      leftMotor->run(RELEASE);
      delay(500);
}

int getPosition(){
  sensor3 = analogRead(sensorPin3);
  sensor2 = analogRead(sensorPin2);
  sensor4 = analogRead(sensorPin4);
  sensor1 = analogRead(sensorPin1);
  sensor5 = analogRead(sensorPin5);
  Serial.println(" ");
  Serial.println(sensor1);
  Serial.println(sensor2);
  Serial.println(sensor3);
  Serial.println(sensor4);
  Serial.println(sensor5);
  sensor1 /= lineSensitivity;
  sensor2 /= lineSensitivity;
  sensor3 /= lineSensitivity;
  sensor4 /= lineSensitivity;
  sensor5 /= lineSensitivity;
  if((sensor1+sensor2+sensor3+sensor4+sensor5)>2){
//    Serial.println("At an intersection");
    return 0;
  }
  else if(sensor2==0&&sensor3==1&&sensor4==0){
//    Serial.println("Dead Center: Line sensor 3");
    return 5;
  }
  else if(sensor2==1&&sensor3==1){
//    Serial.println("Between 2 & 3");
    return 4;
  }
  else if(sensor3==1&&sensor4==1){
//    Serial.println("Between 3 & 4");
    return 6;
  }
  else if(sensor1==0&&sensor2==1&&sensor3==0){
//    Serial.println("Line sensor 2");
    return 3;
  }
  else if(sensor3==0&&sensor4==1&&sensor5==0){
//    Serial.println("Line sensor 4");
    return 7;
  } 
  else if(sensor1==1&&sensor2==1){
//    Serial.println("Between 1 & 2");
    return 2;
  } 
  else if(sensor4==1&&sensor5==1){
//    Serial.println("Between 4 & 5");
    return 8;
  } 
  else if(sensor1==1&&sensor2==0){
//    Serial.println("Line sensor 1");
    return 1;
  }  
  else if(sensor4==0&&sensor5==1){
//    Serial.println("Line sensor 5");
    return 9;
  }
  else{
//    Serial.println("No Line");
    return 10;
  }
}

void rotateLeft(int lineCount){
  // Rotate the robot CCW until given number of lines is crossed.
  leftMotor->setSpeed(100); // Range of 0-255
  rightMotor->setSpeed(100);
  leftMotor->run(BACKWARD);
  rightMotor->run(FORWARD);
  while(lineCount>0){
    delay(100);
    while(analogRead(sensorPin2)<lineSensitivity){}
    lineCount--;
  }
  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
  return;
}

void rotateRight(int lineCount){
  // Rotate the robot CW.
  leftMotor->setSpeed(LMotorSpeed); // Range of 0-255
  rightMotor->setSpeed(RMotorSpeed);
  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);
  while(lineCount>0){
    delay(100);
    while(analogRead(sensorPin4)<lineSensitivity){}
    lineCount--;
  }
  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
  return;
}

//void wheelEncoderISR(){
//  encoderCount++;
//}

int isToken(){
  sensor3 = analogRead(sensorPin3);
  if(sensor3 <= tokenSensitivity && sensor3 > 500 ){
    Serial.println("Token present.");
    return 1;
  }
  else{
    Serial.println("Token not present.");
    return 0;
  }
 }

