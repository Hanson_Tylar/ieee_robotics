#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

#define LMotorSpeed 105
#define RMotorSpeed 98
#define LencoderIntPin 2 //interrupt pin
#define metalDetectorPin 3
#define lineSensitivity 700 // 1 - will always see line, 1000+ - will never see line
#define tokenSensitivity 900
#define WAIT_FOR_START 0
#define LEG_01 1
#define LEG_02 2
#define LEG_03 3
#define LEG_04 4
#define LEG_05 5
#define LEG_06 6
#define LEG_07 7
#define LEG_08 8
#define LEG_09 9
#define LEG_10 10
#define LEG_11 11
#define LEG_12 12
#define LEG_13 13
#define LEG_14 14
#define LEG_15 15
#define LEG_16 16
#define LEG_17 17
#define LEG_18 18
#define LEG_19 19
#define LEG_20 20
#define LEG_21 21
#define LEG_22 22
#define LEG_23 23
#define LEG_24 24
#define LEG_25 25
#define LEG_26 26
#define LEG_27 27
#define LEG_28 28
#define LEG_29 29
#define LEG_30 30
#define LEG_31 31
#define LEG_32 32
#define LEG_33 33
#define LEG_34 34
#define LEG_35 35
#define LEG_36 36
#define LEG_37 37
#define LEG_38 38
#define LEG_39 39
#define LEG_40 40
#define END 50
#define ledPin 33

// Motor Shield initialization
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);

// Line Follower Sensors and variables
int LS0 = A2;
int LS1 = A1;
int LS2 = A0;
int LS3 = A7;
int LS4 = A9;
int LS5 = A15;
int LS6 = A13;
int LS7 = A8;
int LS8 = A14;

int LS0Val = 0;  
int LS1Val = 0;
int LS2Val = 0;
int LS3Val = 0;
int LS4Val = 0;
int LS5Val = 0;
int LS6Val = 0;

int lineCount = 0;
int position = 0;
int tokenFound = 0;
int token = 0;

// Wheel encoder variables
int LencoderCount = 0;

// FSM variables
int state, nextState;

int getPosition();
void followLine();
void rotateLeft(int x);
void rotateRight(int x);
void LwheelEncoderISR();
void tokenFoundISR();
void align(int sensor);

void setup() {
  Serial.begin(9600); // set up Serial library at 9600 bps
  Serial.println("IEEE Robot Mobility Test");
  AFMS.begin(); // create with the default frequency 1.6KHz
  attachInterrupt(digitalPinToInterrupt(LencoderIntPin), LwheelEncoderISR, CHANGE);
  attachInterrupt(digitalPinToInterrupt(metalDetectorPin), tokenFoundISR, FALLING);
  pinMode(ledPin,OUTPUT);
  digitalWrite(ledPin, LOW);
  leftMotor->setSpeed(LMotorSpeed); // Range of 0-255
  rightMotor->setSpeed(RMotorSpeed);
  leftMotor->run(RELEASE);
  rightMotor->run(RELEASE);
  state = WAIT_FOR_START;
  if(state==END){
    leftMotor->setSpeed(80);
    rightMotor->setSpeed(75);
    leftMotor->run(BACKWARD);
    rightMotor->run(BACKWARD);
    delay(3000);
    leftMotor->run(RELEASE);
    rightMotor->run(RELEASE);
  }
}

void loop() {
  switch(state){
    case WAIT_FOR_START:
      // Todo: Wait for go button to be pressed.
      delay(2000);
      nextState = LEG_01;
      break;
      
    case LEG_01:
      leftMotor->setSpeed(LMotorSpeed);
      rightMotor->setSpeed(RMotorSpeed);
      leftMotor->run(BACKWARD);
      rightMotor->run(BACKWARD);
      while(analogRead(LS4)<lineSensitivity){}
      delay(500);
      while(analogRead(LS4)<lineSensitivity){}
      rotateRight(1);
      nextState = LEG_02;
      break;

    case LEG_02:
      followLine();
      align(LS3);
      rotateRight(2);
      nextState = LEG_03;
      break;
      
    case LEG_03:
      followLine();
      // TODO: IF isToken
      align(LS3);
      nextState = LEG_04;
      break;
      
    case LEG_04:
      followLine();
      // TODO: IF isToken
      align(LS3);
      nextState = LEG_05;
      break;
      
    case LEG_05:
      followLine();
      // TODO: IF isToken
      align(LS3);
      rotateLeft(1);
      nextState = LEG_06;
      break;
      
    case LEG_06:
      followLine();
      align(LS4);
      rotateLeft(1);
      nextState = LEG_07;
      break;
      
    case LEG_07:
      followLine();
      align(LS4);
      nextState = LEG_08;
      break;
    case LEG_08:
      followLine();
      align(LS4);
      nextState = LEG_09;
      break;
    case LEG_09:
    // Magenta Square
      followLine();
      align(LS4);      
      rotateRight(1);
      nextState = LEG_10;
      break;
    case LEG_10:
      // Yellow Square
      followLine();
      align(LS3);
      rotateRight(2);
      nextState = LEG_11;
      break;
    case LEG_11:
      followLine();
      align(LS3);
      nextState = LEG_12;
      break;
    case LEG_12:
      followLine();
      align(LS3);
      nextState = LEG_13;
      break; 
    case LEG_13:
      followLine();
      align(LS3);
      rotateLeft(1);
      nextState = LEG_14;
      break;
    case LEG_14:
      followLine();
      align(LS3);
      rotateLeft(1);  
      nextState = LEG_15;
      break;
    case LEG_15:
      followLine();
      align(LS7);
      nextState = LEG_16;
      break;
    case LEG_16:
      followLine();
      align(LS7);
      nextState = LEG_17;
      break;
    case LEG_17:
      // Blue Square
      followLine();
      align(LS7);
      rotateRight(1);  
      nextState = LEG_18;
      break;
    case LEG_18:
      //Green Square
      followLine();
      align(LS4);
      rotateRight(1); 
      nextState = LEG_19;
      break;
    case LEG_19:
      followLine();
      align(LS4);
      nextState = LEG_20;
      break;
    case LEG_20:
      followLine();
      align(LS4);
      nextState = LEG_21;
      break; 
    case LEG_21:
      followLine();
      align(LS4);
      rotateLeft(1);
      nextState = LEG_22;
      break; 
    case LEG_22:
      // Diagonal from Gray to Red
      followLine();
      align(LS3);
      rotateLeft(1);
      nextState=LEG_23;
      break;
    case LEG_23:
      followLine();
      align(LS7);
      nextState=LEG_24;
      break;
    case LEG_24:
      followLine();
      align(LS7);
      nextState=LEG_25;
      break;
    case LEG_25:
      //Last Token
      followLine();
      align(LS7);
      nextState=LEG_26;
      break;
    case LEG_26:
      align(LS3);
      // TODO: Dropoff Red Tokens
      nextState = LEG_27;
      break;
    case LEG_27:
      align(LS7);
      rotateLeft(1);
      nextState = LEG_28;
      break;
    case LEG_28:
      followLine();
      align(LS4); 
      rotateRight(1);
      // TODO: Dropoff Green
      rotateLeft(1);
      nextState = LEG_29;
      break;
    case LEG_29:
      followLine();
      align(LS5);
      rotateRight(1);
      align(LS5);
      nextState = LEG_30;
      break;
    case LEG_30:
      align(LS7);
      rotateLeft(2);
      nextState = LEG_31;  
      break;
    case LEG_31:
      followLine();
      align(LS5);
      followLine();
      align(LS5);
      followLine();
      align(LS5);
      followLine();
      align(LS5);
      // TODO: Dropoff Grey
      rotateRight(1);
      followLine();
      align(LS5);
      rotateRight(1);
      nextState = LEG_32;
      break;
    case LEG_32:
      followLine();
      align(LS7);
      followLine();
      align(LS7);
      followLine();
      align(LS7);
      followLine();
      align(LS7);
      
      align(LS3);
      // TODO: Dropoff Yellow
      nextState = LEG_33;
      break;
    case LEG_33:
      align(LS7);
      rotateLeft(1);
      nextState = LEG_34;
      break;
    case LEG_34:
      followLine();
      align(LS4);
      rotateRight(1);
      //TODO: Dropoff Magenta
      rotateLeft(1);
      nextState = LEG_35;
      break;
    case LEG_35:
      followLine();
      align(LS5);
      rotateRight(1);
      align(LS5);
      //TODO: Dropoff Cyan
      nextState = LEG_36;
      break;
    case LEG_36:
      align(LS7);
      rotateLeft(1);
      nextState = LEG_37;
      break;
    case LEG_37:
      followLine();
      // From final dropoff to white square
      nextState = END;
      break;
    case END:
      Serial.println("END");
      leftMotor -> run(RELEASE);
      rightMotor -> run(RELEASE);
      while(1);
      break;
  }
  state = nextState;
  }

void followLine(){
  position = 0;
  LencoderCount = 0;
  while(position!=6){ 
    position = getPosition();
    switch(position){
      case 0: // No Line
        leftMotor->setSpeed(LMotorSpeed);
        rightMotor->setSpeed(RMotorSpeed);
        break;
      case 1: // Far Right
        leftMotor->setSpeed(LMotorSpeed+10);
        rightMotor->setSpeed(RMotorSpeed-10);
        break;
      case 2: // Center
        leftMotor->setSpeed(LMotorSpeed);
        rightMotor->setSpeed(RMotorSpeed);
        break;
      case 3: // Mid Right
        leftMotor->setSpeed(LMotorSpeed+5);
        rightMotor->setSpeed(RMotorSpeed-5);
        break;
      case 4: // Far Left
        leftMotor->setSpeed(LMotorSpeed-10);
        rightMotor->setSpeed(RMotorSpeed+10);
        break;
      case 5: // Mid Left
        leftMotor->setSpeed(LMotorSpeed-10);
        rightMotor->setSpeed(RMotorSpeed+10);
        break;
      default:
        leftMotor->setSpeed(LMotorSpeed);
        rightMotor->setSpeed(RMotorSpeed);
        break;
    }
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  if(state==37&&LencoderCount>2000){
    break;
  }
  }
  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
  leftMotor->setSpeed(LMotorSpeed);
  rightMotor->setSpeed(RMotorSpeed);
}

int getPosition(){
  LS0Val = analogRead(LS0);
  LS1Val = analogRead(LS1);
  LS2Val = analogRead(LS2);
  LS3Val = analogRead(LS3);
  LS4Val = analogRead(LS4);
  LS5Val = analogRead(LS5);
  LS6Val = analogRead(LS6);  
  Serial.print(LS0Val);Serial.print("\t");
  Serial.print(LS1Val);Serial.print("\t");
  Serial.println(LS2Val);
  Serial.print("LS3: ");Serial.print(LS3Val);Serial.print("\t");
  Serial.print("LS5: ");Serial.println(LS5Val);
  Serial.print("LS4: ");Serial.print(LS4Val);Serial.print("\t");
  Serial.print("LS6: ");Serial.print(LS6Val);Serial.println("\n");
  LS0Val /= lineSensitivity;
  LS1Val /= lineSensitivity;
  LS2Val /= lineSensitivity;
  LS3Val /= lineSensitivity;
  LS4Val /= lineSensitivity;
  LS5Val /= lineSensitivity;
  LS6Val /= lineSensitivity;

  if(LS0Val==0&&LS1Val==0&&LS2Val==0){
    // No line
    return 0;
  }
  else if(LS0Val==0&&LS1Val==0&&LS2Val==1){
    // Far Right
    return 1;
  }
  else if(LS0Val==0&&LS1Val==1&&LS2Val==0){
    // Center
    return 2;
  }
  else if(LS0Val==0&&LS1Val==1&&LS2Val==1){
    // Mid right
    return 3;
  }
  else if(LS0Val==1&&LS1Val==0&&LS2Val==0){
    // Far Left
    return 4;
  }
  else if(LS0Val==1&&LS1Val==1&&LS2Val==0){
    // Mid Left
    return 5;
  }
  else if(LS0Val==1&&LS1Val==1&&LS2Val==1){
    // Intersection
    return 6;
  }
  else{
    return 0;
  }
}

void align(int sensor){
  if(state==27||state==30||state==33||state==36){
    leftMotor->setSpeed(70);
    rightMotor->setSpeed(65);
    leftMotor->run(BACKWARD);
    rightMotor->run(BACKWARD);
  }
  else{
    leftMotor->setSpeed(70);
    rightMotor->setSpeed(65);
    leftMotor->run(FORWARD);
    rightMotor->run(FORWARD);
  }
  while(analogRead(sensor)<900){}
  leftMotor->run(RELEASE);
  rightMotor->run(RELEASE);

  position = getPosition();
  leftMotor->setSpeed(LMotorSpeed-30);
  rightMotor->setSpeed(RMotorSpeed-30);
  if(position==1){
    leftMotor->run(FORWARD);
    rightMotor->run(BACKWARD);
    delay(75);
  }
  else if(position==4){
    leftMotor->run(BACKWARD);
    rightMotor->run(FORWARD);
    delay(75);
  }
  leftMotor->run(RELEASE);
  rightMotor->run(RELEASE);
  if(tokenFound==1&&state<26){
    digitalWrite(ledPin,HIGH);
    delay(7000); //Simulate Token Pickup and Storage
    tokenFound=0;
    digitalWrite(ledPin,LOW);
  }
}

void rotateLeft(int lineCount){
  // Rotate the robot CCW until given number of lines is crossed.
  leftMotor->setSpeed(95); // Range of 0-255
  rightMotor->setSpeed(85);
  leftMotor->run(BACKWARD);
  rightMotor->run(FORWARD);
  while(lineCount>0){
    delay(75);
    while(getPosition()!=4){}
    lineCount--;
  }
  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
  return;
}

void rotateRight(int lineCount){
  // Rotate the robot CW.
  leftMotor->setSpeed(90); // Range of 0-255
  rightMotor->setSpeed(80);
  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);
  while(lineCount>0){
    delay(75);
    while(getPosition()!=1){}
    lineCount--;
  }
  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
  return;
}

void LwheelEncoderISR(){
  LencoderCount = (LencoderCount+1) % 30000;
}

void tokenFoundISR(){
  tokenFound = 1;
}

