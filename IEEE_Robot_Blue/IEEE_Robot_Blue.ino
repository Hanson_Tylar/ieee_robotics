/* IEEE Robot Mobility Subsystem
Author: Tylar J Hanson
School: USAFA
Porject: IEEE Robotics Comepetition
Completed Date: NLT 7 April, 2018
*/

#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <Servo.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include "Adafruit_TCS34725.h"

#define LMotorSpeed 130
#define RMotorSpeed 135

// Analog Sensors <-> Analog Pins
#define LS0 A2
#define LS1 A1
#define LS2 A0
#define LS3 A7
#define LS4 A9
#define LS5 A15
#define LS6 A13
#define LS7 A8
#define LS8 A14
#define lineSensitivity 800 // 1 will always see line, 1000+ will never see line

#define LencoderIntPin 2 //interrupt pin
#define metalDetectorPin 3
#define ledPin 33
#define relay 22

#define WAIT_FOR_START 0
#define LEG_01 1
#define LEG_02 2
#define LEG_03 3
#define LEG_04 4
#define LEG_05 5
#define LEG_06 6
#define LEG_07 7
#define LEG_08 8
#define LEG_09 9
#define LEG_10 10
#define LEG_11 11
#define LEG_12 12
#define LEG_13 13
#define LEG_14 14
#define LEG_15 15
#define LEG_16 16
#define LEG_17 17
#define LEG_18 18
#define LEG_19 19
#define LEG_20 20
#define LEG_21 21
#define LEG_22 22
#define LEG_23 23
#define LEG_24 24
#define LEG_25 25
#define LEG_26 26
#define LEG_27 27
#define LEG_28 28
#define LEG_29 29
#define LEG_30 30
#define LEG_31 31
#define LEG_32 32
#define LEG_33 33
#define LEG_34 34
#define LEG_35 35
#define LEG_36 36
#define LEG_37 37
#define LEG_38 38
#define LEG_39 39
#define LEG_40 40
#define END 41

// Motor Shield initialization
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);
Servo mylinearactuator;  // linear actuator object
Servo myservo;
Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_4X);

int position = 0;
int tokenFound = 0;
int state = 0;
int ROUND = 1;

// Wheel encoder variables
int LencoderCount = 0;

void setup(){
  Serial.begin(9600); // set up Serial library at 9600 bps
  Serial.println("IEEE Robot Integration Testing");
  AFMS.begin(); // create with the default frequency 1.6KHz
  attachInterrupt(digitalPinToInterrupt(LencoderIntPin), LwheelEncoderISR, CHANGE);
  attachInterrupt(digitalPinToInterrupt(metalDetectorPin), tokenFoundISR, FALLING);
  pinMode(relay, OUTPUT); // Digitial signal for relay/electromagent
  pinMode(ledPin,OUTPUT);
  digitalWrite(ledPin, LOW);
  mylinearactuator.attach(9);  // attaches the linear actuator on pin 9 
  myservo.attach(10); //attaches the servo on pin 10
  extend();
//  Drop();
  blue();
  delay(500);
  leftMotor->setSpeed(LMotorSpeed); // Range of 0-255
  rightMotor->setSpeed(RMotorSpeed);
  leftMotor->run(RELEASE);
  rightMotor->run(RELEASE);
  state = WAIT_FOR_START;
}
void loop(){
  switch(state){
    case WAIT_FOR_START:
      break;     
    case LEG_01:
      LencoderCount = 0;
      leftMotor->setSpeed(LMotorSpeed); // Range of 0-255
      rightMotor->setSpeed(RMotorSpeed);
      leftMotor->run(BACKWARD);
      rightMotor->run(BACKWARD);
      while(LencoderCount<130){Serial.println(LencoderCount);}
      rotateRight(1);
      break;
    case LEG_02:
      followLine();
      align(LS3);
      rotateRight(2);
      break;
    case LEG_03:
      followLine();
      align(LS3);
      break;  
    case LEG_04:
      followLine();
      align(LS3);
      break;      
    case LEG_05:
      followLine();
      align(LS3);
      rotateLeft(1);
      break;
    case LEG_06:
      followLine();
      align(LS4);
      rotateLeft(1);
      break; 
    case LEG_07:
      followLine();
      align(LS4);
      break;
    case LEG_08:
      followLine();
      align(LS4);
      break;
    case LEG_09:
    // Magenta Square
      followLine();
      align(LS4);      
      rotateRight(1);
      break;
    case LEG_10:
      // Yellow Square
      followLine();
      align(LS3);
      rotateRight(2);
      break;
    case LEG_11:
      followLine();
      align(LS3);
      break;
    case LEG_12:
      followLine();
      align(LS3);
      break; 
    case LEG_13:
      followLine();
      align(LS3);
      rotateLeft(1);
      break;
    case LEG_14:
      followLine();
      align(LS3);
      rotateLeft(1);  
      break;
    case LEG_15:
      followLine();
      align(LS7);
      break;
    case LEG_16:
      followLine();
      align(LS7);
      break;
    case LEG_17:
      // Blue Square
      followLine();
      align(LS7);
      rotateRight(1);  
      break;
    case LEG_18:
      //Green Square
      followLine();
      align(LS4);
      rotateRight(1); 
      break;
    case LEG_19:
      followLine();
      align(LS4);
      break;
    case LEG_20:
      followLine();
      align(LS4);
      break; 
    case LEG_21:
      followLine();
      align(LS4);
      rotateLeft(1);
      break; 
    case LEG_22:
      // Diagonal from Gray to Red
      followLine();
      align(LS3);
      rotateLeft(1);
      break;
    case LEG_23:
      followLine();
      align(LS7);
      break;
    case LEG_24:
      followLine();
      align(LS7);
      break;
    case LEG_25:
      //Last Token
      followLine();
      align(LS7);
      mylinearactuator.writeMicroseconds(1260);
      break;
    case LEG_26:
      align(LS3);
      drop_red();
      break;
    case LEG_27:
      align(LS7);
      rotateLeft(1);
      mylinearactuator.detach();
      break;
    case LEG_28:
      followLine();
      align(LS4); 
      rotateRight(1);
      drop_green();
      rotateLeft(1);
      break;
    case LEG_29:
      followLine();
      align(LS5);
      rotateRight(1);
      break;
    case LEG_30:
      align(LS5);
      drop_blue();
      break;
    case LEG_31:
      align(LS7);
      if(ROUND==3){
        rotateLeft(2);
      }
      else{
        rotateLeft(1);
        followLine();
        state=32;
      }
      break;
    case LEG_32:
      followLine();
      align(LS5);
      followLine();
      align(LS5);
      followLine();
      align(LS5);
      rotateRight(1);
      followLine();
      rotateLeft90();
      drop_gray();
      rotateRight(1);
      followLine();
      align(LS5);
      rotateRight(1);
      break;
    case LEG_33:
      if(ROUND==3){
        followLine();
        align(LS7);
        followLine();
        align(LS7);
        followLine();
        align(LS7);  
      }
      else{
        align(LS5);
        rotateRight(1);
      }
      break;
    case LEG_34:
      align(LS3);
      drop_yellow();
      break;
    case LEG_35:
      align(LS7);
      rotateLeft(1);
      break;
    case LEG_36:
      followLine();
      align(LS4);
      rotateRight(1);
      drop_magenta();
      rotateLeft(1);
      break;
    case LEG_37:
      followLine();
      align(LS5);
      rotateRight(1);
      break;
    case LEG_38:
      align(LS5);
      drop_cyan();
      break;
    case LEG_39:
      align(LS7);
      rotateLeft(1);
      break;
    case LEG_40:
      // From cyan square to white square
      followLine();
      rotateRight90();
      leftMotor->setSpeed(LMotorSpeed); // Range of 0-255
      rightMotor->setSpeed(RMotorSpeed);
      LencoderCount = 0;
      leftMotor->run(FORWARD);
      rightMotor->run(FORWARD);
      while(LencoderCount<1650){
        Serial.print(LencoderCount);
      }
      leftMotor->run(RELEASE);
      rightMotor->run(RELEASE);
      break;
    case END:
      leftMotor -> run(RELEASE);
      rightMotor -> run(RELEASE);
      while(1);
      break;
  }
  state++;
  }
void followLine(){
  LencoderCount = 0;
  int LencoderRestoreVal = 0;
  while(getPosition()!=6){ 
    switch(position){
      case 0: // No Line, rotate left a little bit, if no line found still, rotate right until line found
        digitalWrite(ledPin,HIGH);
        leftMotor->run(RELEASE);
        rightMotor->run(RELEASE);
        leftMotor->setSpeed(84);
        rightMotor->setSpeed(80);
        LencoderRestoreVal = LencoderCount; // Save previous value of encoder for restoration after line is found
        LencoderCount = 0;
        leftMotor->run(BACKWARD);
        rightMotor->run(FORWARD);
        while(getPosition()==0&&LencoderCount<400){}
        if(position==0){
          leftMotor->setSpeed(87);
          rightMotor->setSpeed(83);
          leftMotor->run(FORWARD);
          rightMotor->run(BACKWARD);
          while(getPosition()==0){}
        }
        leftMotor->run(RELEASE);
        rightMotor->run(RELEASE);
        LencoderCount = LencoderRestoreVal;
        digitalWrite(ledPin,LOW);
        break;
      case 1: // Far Right
        leftMotor->setSpeed(LMotorSpeed+20);
        rightMotor->setSpeed(RMotorSpeed-20);
        break;
      case 2: // Center
        leftMotor->setSpeed(LMotorSpeed);
        rightMotor->setSpeed(RMotorSpeed);
        break;
      case 3: // Mid Right
        leftMotor->setSpeed(LMotorSpeed+10);
        rightMotor->setSpeed(RMotorSpeed-10);
        break;
      case 4: // Far Left
        leftMotor->setSpeed(LMotorSpeed-20);
        rightMotor->setSpeed(RMotorSpeed+20);
        break;
      case 5: // Mid Left
        leftMotor->setSpeed(LMotorSpeed-10);
        rightMotor->setSpeed(RMotorSpeed+10);
        break;
      default:
        leftMotor->setSpeed(LMotorSpeed);
        rightMotor->setSpeed(RMotorSpeed);
        break;
    }
    leftMotor->run(FORWARD);
    rightMotor->run(FORWARD);
    if(state==32&&LencoderCount>3000){
      break;
    }
    if(state==40&&LencoderCount>7200){
      break;
    }
  }
  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
  leftMotor->setSpeed(LMotorSpeed);
  rightMotor->setSpeed(RMotorSpeed);
}
int getPosition(){
  int LS0Val = analogRead(LS0);  
  int LS1Val = analogRead(LS1);
  int LS2Val = analogRead(LS2);
//  int LS3Val = analogRead(LS3);
//  int LS4Val = analogRead(LS4);
//  int LS5Val = analogRead(LS5);
//  int LS6Val = analogRead(LS6);
//  int LS7Val = analogRead(LS7);
//  int LS8Val = analogRead(LS8);
//  Serial.print(LS0Val);Serial.print("\t");
//  Serial.print(LS1Val);Serial.print("\t");
//  Serial.println(LS2Val);
//  Serial.print("LS3: ");Serial.print(LS3Val);Serial.print("\t\t");
//  Serial.print("LS5: ");Serial.println(LS5Val);
//  Serial.print("LS4: ");Serial.print(LS4Val);Serial.print("\t\t");
//  Serial.print("LS6: ");Serial.println(LS6Val);
//  Serial.print("LS7: ");Serial.print(LS7Val);Serial.print("\t\t");
//  Serial.print("LS8: ");Serial.print(LS8Val);Serial.println("\n");
  LS0Val /= lineSensitivity;
  LS1Val /= lineSensitivity;
  LS2Val /= lineSensitivity;
//  LS3Val /= lineSensitivity;
//  LS4Val /= lineSensitivity;
//  LS5Val /= lineSensitivity;
//  LS6Val /= lineSensitivity;  
//  LS7Val /= lineSensitivity;
//  LS8Val /= lineSensitivity;
  if(LS0Val==0&&LS1Val==0&&LS2Val==0){
    // No line
    position = 0;
    return 0;
  }
  else if(LS0Val==0&&LS1Val==0&&LS2Val==1){
    // Far Right
    position = 1;
    return 1;
  }
  else if(LS0Val==0&&LS1Val==1&&LS2Val==0){
    // Center
    position = 2;
    return 2;
  }
  else if(LS0Val==0&&LS1Val==1&&LS2Val==1){
    // Mid right
    position = 3;
    return 3;
  }
  else if(LS0Val==1&&LS1Val==0&&LS2Val==0){
    // Far Left
    position = 4;
    return 4;
  }
  else if(LS0Val==1&&LS1Val==1&&LS2Val==0){
    // Mid Left
    position = 5;
    return 5;
  }
  else if(LS0Val==1&&LS1Val==1&&LS2Val==1){
    // Intersection
    position = 6;
    return 6;
  }
  else{
    position = 0;
    return 0;
  }
}
void align(int sensor){
  if(state==1||state==27||state==31||state==35||state==39){
    leftMotor->setSpeed(100);
    rightMotor->setSpeed(95);
    leftMotor->run(BACKWARD);
    rightMotor->run(BACKWARD);
  }
  else{
    leftMotor->setSpeed(LMotorSpeed-40);//78);
    rightMotor->setSpeed(LMotorSpeed-37);//75);
    leftMotor->run(FORWARD);
    rightMotor->run(FORWARD);
  }
  LencoderCount = 0;
  int dist = 0;
  if(state==26||state==27||state==30||state==31||state==34||state==35||state==38||state==39){
    dist = 150;
  }
  else{
    dist = 111;
  }
  while(LencoderCount<dist&&tokenFound==0){Serial.print(LencoderCount);}
//  while(analogRead(sensor)<lineSensitivity&&tokenFound==0){}
  if(tokenFound==1){
    LencoderCount = 0;
    
    if(state==2||state==10||state==14||state==22){
      dist = 50;
    }
    else if((state>=3&&state<=5)||(state>=11&&state<=13)){
      dist = 73;
    }
    else if((state>=6&&state<=9)||(state>=18&&state<=21)){
      dist = 71;
    }
    else if((state>=15&&state<=17)||(state>=23&&state<=25)){
      dist = 70;
    }
    else{
      dist = 70;
    }
    while(LencoderCount<dist){Serial.println(LencoderCount);}
  }
  leftMotor->run(RELEASE);
  rightMotor->run(RELEASE);
  delay(100);  
  if(tokenFound==1&&state<26){//TODO: Do we want to pick up missed tokens while doing droppoff routine?
    digitalWrite(ledPin,HIGH);
    if(state!=2&&state!=3&&state!=6&&state!=7&&state!=10&&state!=11&&state!=14&&state!=18&&state!=19&&state!=22){
      // These legs are long enough for linear actuator to extend fully without a delay
      delay(2000);
    }
    Token();
    tokenFound=0;
    digitalWrite(ledPin,LOW);
  }
  while(1){};
}
void rotateLeft(int lineCount){
  // Rotate the robot CCW using line sensors.
  leftMotor->setSpeed(115); // Range of 0-255
  rightMotor->setSpeed(115);
  leftMotor->run(BACKWARD);
  rightMotor->run(FORWARD);
  LencoderCount = 0;
  while(LencoderCount<400){Serial.print(LencoderCount);}
  while(lineCount>0){
    while(getPosition()!=4){}
    lineCount--;
    if(lineCount>0){
      LencoderCount = 0;
      while(LencoderCount<400){Serial.print(LencoderCount);}
    }    
  }
  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
  return;
}
void rotateRight(int lineCount){
  // Rotate the robot CW using the line sensors.
  leftMotor->setSpeed(115); // Range of 0-255
  rightMotor->setSpeed(125);
  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);
  LencoderCount = 0;
  while(LencoderCount<40){Serial.print(LencoderCount);}
  while(lineCount>0){
    while(getPosition()!=1){}
//    lineCount--;
    if(--lineCount>0){
      LencoderCount = 0;
      while(LencoderCount<40){Serial.print(LencoderCount);}
    }    
  }
  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
  return;
}
void rotateRight90(){
  // Rotate the robot CW 90 degrees using wheel encoders.
  leftMotor->setSpeed(115); // Range of 0-255
  rightMotor->setSpeed(125);
  LencoderCount = 0;
  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);
  while(LencoderCount<1700){
    Serial.println(LencoderCount);
  }
  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
  return;
}
void rotateLeft90(){
  // Rotate the robot CW 90 degrees, using wheel encoders.
  leftMotor->setSpeed(97); // Range of 0-255
  rightMotor->setSpeed(93);
  LencoderCount = 0;
  leftMotor->run(BACKWARD);
  rightMotor->run(FORWARD);
  while(LencoderCount<1875){
    Serial.println(LencoderCount);
  }
  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
  return;
}
void LwheelEncoderISR(){
  LencoderCount = (LencoderCount+1) % 30000;
}
void tokenFoundISR(){
  tokenFound = 1;
}
