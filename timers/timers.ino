#define ledPin 33

int pickupReady = 0;

void setup() {
  pinMode(ledPin, OUTPUT);
  noInterrupts();           // disable all interrupts
  TCCR2A = 0;
  TCCR2B = 0;
  TCNT2  = 0;
  OCR2A = 256;            // compare match register 16MHz/256/1Hz
  TCCR2B |= (1 << WGM12);   // CTC mode
  TCCR2B |= (1 << CS12);    // 256 prescaler 
  TCCR2B |= (1 << CS10);
  TIMSK2 |= (1 << OCIE2A);  // enable timer compare interrupt
  interrupts();             // enable all interrupts
}

void loop() {
  // put your main code here, to run repeatedly:
  if(pickupReady==1){
    
  }
}
ISR(TIMER2_COMPA_vect){
  digitalWrite(ledPin, digitalRead(ledPin) ^ 1);   // toggle LED pin
}
