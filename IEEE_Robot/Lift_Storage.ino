/* TOKEN Subsystem
Author: Lucas E Mireles
School: USAFA
Porject: IEEE Robotics Comepetition
Completed Date: NLT April 7th, 2018
*/

#define RETRACT     1290
#define DETECT      1500 //1400
#define EXTEND      2000
#define RED         595
#define BLUE        880
#define GREEN       745
#define YELLOW      1020
#define CYAN        1305
#define MAGENTA     1160
#define GRAY        1465
#define DROPRED     1545
#define DROPBLUE    1865
#define DROPGREEN   1720
#define DROPYELLOW  2005
#define DROPCYAN    2260
#define DROPMAGENTA 2135
#define DROPGRAY    2385

#define RED0 0
#define GREEN1 1
#define BLUE2 2
#define YELLOW3 3
#define MAGENTA4 4
#define CYAN5 5
#define GRAY6 6


#define RED_R_MAX 2217
#define RED_R_MIN 2139
#define RED_G_MAX 637
#define RED_G_MIN 603
#define RED_B_MAX 636
#define RED_B_MIN 589
#define GREEN_R_MAX 2400
#define GREEN_R_MIN 2366
#define GREEN_G_MAX 3434
#define GREEN_G_MIN 3300
#define GREEN_B_MAX 1874
#define GREEN_B_MIN 1834
#define BLUE_R_MAX 1126
#define BLUE_R_MIN 981
#define BLUE_G_MAX 1826
#define BLUE_G_MIN 1591
#define BLUE_B_MAX 2790
#define BLUE_B_MIN 2426
#define YELLOW_R_MAX 7039
#define YELLOW_R_MIN 6933
#define YELLOW_G_MAX 6562
#define YELLOW_G_MIN 6434
#define YELLOW_B_MAX 3734
#define YELLOW_B_MIN 3685
#define MAGENTA_R_MAX 1884
#define MAGENTA_R_MIN 1856
#define MAGENTA_G_MAX 1111
#define MAGENTA_G_MIN 1081
#define MAGENTA_B_MAX 1428
#define MAGENTA_B_MIN 1410
#define CYAN_R_MAX 1719
#define CYAN_R_MIN 1673
#define CYAN_G_MAX 3793
#define CYAN_G_MIN 3738
#define CYAN_B_MAX 4016
#define CYAN_B_MIN 3953
#define GRAY_R_MAX 921
#define GRAY_R_MIN 883
#define GRAY_G_MAX 1038
#define GRAY_G_MIN 1003
#define GRAY_B_MAX 968
#define GRAY_B_MIN 954



//void setup() {
//   Serial.begin(9600);
//  mylinearactuator.attach(9);  // attaches the linear actuator on pin 9 
//  myservo.attach(10); //attaches the servo on pin 10
//  pinMode(22, OUTPUT);
//}

//void loop() {
////mylinearactuator.writeMicroseconds(RETRACT);
//
////Magnet_On();
////readTokenFunc();
////Token();
////delay(2000);
////Magnet_Off();
////red();
////delay(1000);
////green();
////delay(1000);
////blue();
////delay(1000);
////yellow();
////delay(1000);
////magenta();
////delay(1000);
////cyan();
////delay(1000);
////gray();
////delay(1000);
////mylinearactuator.writeMicroseconds(RETRACT);
//myservo.writeMicroseconds(1500);
//Drop();
//}

void Token(){
  Magnet_On();
  color_detection();
  retract();
  Magnet_Off();
  extend();
}
void Magnet_On(){
    Serial.println("Magnet On");
  digitalWrite(22, HIGH);
}
void Magnet_Off(){
  Serial.println("Magnet Off");
  digitalWrite(22, LOW);
  delay(100);
} 
void extend(){
  mylinearactuator.writeMicroseconds(EXTEND);
  Serial.println("\nExtending");
}
void retract(){
//    2.5 seconds from color to retract
  mylinearactuator.writeMicroseconds(RETRACT);
  Serial.println("\nRetracting");
  delay(1500);
}
void color_detection(){
  int token;
  // 6 seconds from bottom to color
  mylinearactuator.writeMicroseconds(DETECT);
  delay(3000);
  token = readTokenFunc();
  Serial.print("Token has been read");
  switch (token){
    case RED0:
      red();
      break;
    case GREEN1:
      green();
      break;
    case BLUE2:
      blue();
      break;
    case YELLOW3:
      yellow();
      break;
    case MAGENTA4:
      magenta();
      break;
    case CYAN5:
      cyan();
      break;
    case GRAY6:
      gray();
      break;
  }
}
void red(){
  myservo.writeMicroseconds(RED);
}
void blue(){
  myservo.writeMicroseconds(BLUE);
}
void yellow(){
  myservo.writeMicroseconds(YELLOW);
}
void green(){
  myservo.writeMicroseconds(GREEN);
}
void cyan(){
  myservo.writeMicroseconds(CYAN);
}
void magenta(){
  myservo.writeMicroseconds(MAGENTA);
}
void gray(){
  myservo.writeMicroseconds(GRAY);
}
void drop_red(){
  myservo.writeMicroseconds(DROPRED);
}
void drop_blue(){
  myservo.writeMicroseconds(DROPBLUE);
}
void drop_yellow(){
  myservo.writeMicroseconds(DROPYELLOW);
}
void drop_green(){
  myservo.writeMicroseconds(DROPGREEN);
}
void drop_cyan(){
  myservo.writeMicroseconds(DROPCYAN);
}
void drop_magenta(){
  myservo.writeMicroseconds(DROPMAGENTA);
}
void drop_gray(){
  myservo.writeMicroseconds(DROPGRAY);
}
void Drop(){
  drop_red();
  delay(500);
  drop_green();
  delay(500);
  drop_blue();
  delay(500);
  drop_yellow();
  delay(500);
  drop_magenta();
  delay(500);
  drop_cyan();
  delay(500);
  drop_gray();
  delay(500);
}
int readTokenFunc(){
  int token=0;
//  int count;
  uint16_t clear, red, green, blue;
//  digitalWrite(ledpin, HIGH);
//  delay(100);
  tcs.getRawData(&red, &green, &blue, &clear);
//  digitalWrite(ledpin, LOW);
  
//  Serial.print("C:\t"); Serial.print(clear);
//  Serial.print("\tR:\t"); Serial.print(red);
//  Serial.print("\tG:\t"); Serial.print(green);
//  Serial.print("\tB:\t"); Serial.print(blue);

  // Figure out some basic hex code for visualization
  uint32_t sum = clear;
  float r, g, b;
  
  r = red; r /= sum;
  g = green; g /= sum;
  b = blue; b /= sum;
  r *= 256; g *= 256; b *= 256;

// MAYBE MAKE A WHILE SO IT MAKES SURE IT GETS A TOKEN
    Serial.print("C:\t"); Serial.print(clear);
    Serial.print("\tR:\t"); Serial.print(r);
    Serial.print("\tG:\t"); Serial.print(g);
    Serial.print("\tB:\t"); Serial.print(b);
    Serial.print("\t");
    Serial.print((int)r, HEX); Serial.print((int)g, HEX); Serial.print((int)b, HEX);
    Serial.println();
    if( r > 150 && g < 70){
      token = RED0;
      Serial.println("Token is Red");
    }
    else if( g > 100 && b < 90 ){
      token = GREEN1;
      Serial.println("Token is Green");
    }
    else if( g < 95 && b > 110 ){
      token = BLUE2;
      Serial.println("Token is Blue");
    }
    else if( r > 95 && g > 85){
      token = YELLOW3;
      Serial.println("Token is Yellow");
    }
    else if( r > 95 && b > 70 ){
      token = MAGENTA4;
      Serial.println("Token is Magenta");
    }
    else if( b > 95 && g > 95 ){
      token = CYAN5;
      Serial.println("Token is Cyan");
    }
    else if( r < 95 && r > 70 && g > 80 ){
      token = GRAY6;
      Serial.println("Token is Gray");
    }
//    else if( count == 25){
//      token = random(6);
//      Serial.println("Assigned Color to Token");
//    }
//    count += 1;
    return token;
}
