/* TOKEN Subsystem
Author: Lucas E Mireles
School: USAFA
Porject: IEEE Robotics Comepetition
Completed Date: NLT April 7th, 2018
*/

#define RETRACT     1255
#define DETECT      1545 //1500
#define EXTEND      1980
#define RED         500
#define GREEN       670
#define BLUE        810
#define GRAY        950 
#define YELLOW      1080
#define MAGENTA     1220
#define CYAN        1370
#define DROPRED     1550
#define DROPGREEN   1730
#define DROPBLUE    1875
#define DROPGRAY    2015
#define DROPYELLOW  2145
#define DROPMAGENTA 2275
#define DROPCYAN    2500

#define RED0 0
#define GREEN1 1
#define BLUE2 2
#define YELLOW3 3
#define MAGENTA4 4
#define CYAN5 5
#define GRAY6 6

#define RED_R_MAX 2217
#define RED_R_MIN 2139
#define RED_G_MAX 637
#define RED_G_MIN 603
#define RED_B_MAX 636
#define RED_B_MIN 589
#define GREEN_R_MAX 2400
#define GREEN_R_MIN 2366
#define GREEN_G_MAX 3434
#define GREEN_G_MIN 3300
#define GREEN_B_MAX 1874
#define GREEN_B_MIN 1834
#define BLUE_R_MAX 1126
#define BLUE_R_MIN 981
#define BLUE_G_MAX 1826
#define BLUE_G_MIN 1591
#define BLUE_B_MAX 2790
#define BLUE_B_MIN 2426
#define YELLOW_R_MAX 7039
#define YELLOW_R_MIN 6933
#define YELLOW_G_MAX 6562
#define YELLOW_G_MIN 6434
#define YELLOW_B_MAX 3734
#define YELLOW_B_MIN 3685
#define MAGENTA_R_MAX 1884
#define MAGENTA_R_MIN 1856
#define MAGENTA_G_MAX 1111
#define MAGENTA_G_MIN 1081
#define MAGENTA_B_MAX 1428
#define MAGENTA_B_MIN 1410
#define CYAN_R_MAX 1719
#define CYAN_R_MIN 1673
#define CYAN_G_MAX 3793
#define CYAN_G_MIN 3738
#define CYAN_B_MAX 4016
#define CYAN_B_MIN 3953
#define GRAY_R_MAX 921
#define GRAY_R_MIN 883
#define GRAY_G_MAX 1038
#define GRAY_G_MIN 1003
#define GRAY_B_MAX 968
#define GRAY_B_MIN 954

void Token(){
  Magnet_On();
  color_detection();
//  retract();
//  Magnet_Off();
  extend();
}

void Magnet_On(){
  digitalWrite(22, HIGH);
}
void Magnet_Off(){
  digitalWrite(22, LOW);
  delay(100);
} 
void extend(){
  mylinearactuator.writeMicroseconds(EXTEND);
}
void retract(){
  mylinearactuator.writeMicroseconds(RETRACT);
  delay(1500);
}
void color_detection(){
  int token;
  mylinearactuator.writeMicroseconds(DETECT);
  delay(3000);
  token = readTokenFunc();
  Serial.print("Token has been read");
  switch (token){
    case RED0:
      red();
      break;
    case GREEN1:
      green();
      break;
    case BLUE2:
      blue();
      break;
    case YELLOW3:
      yellow();
      break;
    case MAGENTA4:
      magenta();
      break;
    case CYAN5:
      cyan();
      break;
    case GRAY6:
      gray();
      break;
  }
  
}
void red(){
  myservo.writeMicroseconds(RED);
  retract();
  Magnet_Off();
  myservo.writeMicroseconds(RED + 60);
  delay(100);
  myservo.writeMicroseconds(RED);
  delay(100);
  myservo.writeMicroseconds(RED + 60);
  delay(100);
  myservo.writeMicroseconds(RED);
  delay(100);
}
void blue(){
  myservo.writeMicroseconds(BLUE);
  retract();
  Magnet_Off();
  myservo.writeMicroseconds(BLUE + 40);
  delay(100);
  myservo.writeMicroseconds(BLUE - 60);
  delay(100);
  myservo.writeMicroseconds(BLUE + 60);
  delay(100);
  myservo.writeMicroseconds(BLUE - 40);
  delay(100);
}
void yellow(){
  myservo.writeMicroseconds(YELLOW);
  retract();
  Magnet_Off();
  myservo.writeMicroseconds(YELLOW + 40);
  delay(100);
  myservo.writeMicroseconds(YELLOW - 60);
  delay(100);
  myservo.writeMicroseconds(YELLOW + 60);
  delay(100);
  myservo.writeMicroseconds(YELLOW - 40);
  delay(100);
}
void green(){
  myservo.writeMicroseconds(GREEN);
  retract();
  Magnet_Off();
  myservo.writeMicroseconds(GREEN + 40);
  delay(100);
  myservo.writeMicroseconds(GREEN - 60);
  delay(100);
  myservo.writeMicroseconds(GREEN + 60);
  delay(100);
  myservo.writeMicroseconds(GREEN - 40);
  delay(100);
}
void cyan(){
  myservo.writeMicroseconds(CYAN);
  retract();
  Magnet_Off();
  myservo.writeMicroseconds(CYAN + 40);
  delay(100);
  myservo.writeMicroseconds(CYAN - 60);
  delay(100);
  myservo.writeMicroseconds(CYAN + 60);
  delay(100);
  myservo.writeMicroseconds(CYAN - 40);
  delay(100);
}
void magenta(){
  myservo.writeMicroseconds(MAGENTA);
  retract();
  Magnet_Off();
  myservo.writeMicroseconds(MAGENTA + 40);
  delay(100);
  myservo.writeMicroseconds(MAGENTA - 60);
  delay(100);
  myservo.writeMicroseconds(MAGENTA + 60);
  delay(100);
  myservo.writeMicroseconds(MAGENTA - 40);
  delay(100);
}
void gray(){
  myservo.writeMicroseconds(GRAY);
  retract();
  Magnet_Off();
  myservo.writeMicroseconds(GRAY + 40);
  delay(100);
  myservo.writeMicroseconds(GRAY - 60);
  delay(100);
  myservo.writeMicroseconds(GRAY + 60);
  delay(100);
  myservo.writeMicroseconds(GRAY - 40);
  delay(100);
}
void drop_red(){
  myservo.writeMicroseconds(DROPRED);
  delay(500);
}
void drop_blue(){
  myservo.writeMicroseconds(DROPBLUE);
  delay(500);
}
void drop_yellow(){
  myservo.writeMicroseconds(DROPYELLOW);
  delay(500);
}
void drop_green(){
  myservo.writeMicroseconds(DROPGREEN);
  delay(500);
}
void drop_cyan(){
  myservo.writeMicroseconds(DROPCYAN);
  delay(500);
}
void drop_magenta(){
  myservo.writeMicroseconds(DROPMAGENTA);
  delay(500);
}
void drop_gray(){
  myservo.writeMicroseconds(DROPGRAY);
  delay(500);
}
void Drop(){
  drop_red();
  drop_green();
  drop_blue();
  drop_gray();
  drop_yellow();
  drop_magenta();
  drop_cyan();
}
int readTokenFunc(){
  int token = 0;
  uint16_t clear, red, green, blue;
  tcs.getRawData(&red, &green, &blue, &clear);

  // Figure out some basic hex code for visualization
  uint32_t sum = clear;
  float r, g, b;
  
  r = red; r /= sum;
  g = green; g /= sum;
  b = blue; b /= sum;
  r *= 256; g *= 256; b *= 256;

// MAYBE MAKE A WHILE SO IT MAKES SURE IT GETS A TOKEN
  Serial.print("C:\t"); Serial.print(clear);
  Serial.print("\tR:\t"); Serial.print(r);
  Serial.print("\tG:\t"); Serial.print(g);
  Serial.print("\tB:\t"); Serial.print(b);
  Serial.print("\t");
  Serial.print((int)r, HEX); Serial.print((int)g, HEX); Serial.print((int)b, HEX);
  Serial.println();
  if( r > 120 && g < 60 && b < 60 ){
    token = RED0;
    Serial.println("Token is Red");
  }
  else if( g > 100 && b < 75 && r < 95){
    token = GREEN1;
    Serial.println("Token is Green");
  }
  else if( b > 90 && r < 65 ){
    if ( g < 95){
      token = BLUE2;
      Serial.println("Token is Blue");
     }
     else{
      token = CYAN5;
      Serial.println("Token is Cyan");
     }
  }
  else if( r > 95 ){
    if( b > 65 && g < 80 ){
     token = MAGENTA4;
     Serial.println("Token is Magenta");}
    else{
      token = YELLOW3;
      Serial.println("Token is Yellow");
     }
  }
  else{
    token = GRAY6;
    Serial.println("Token is Gray");
  }
  return token;
}

void trayCalibration(){
  retract();
  delay(1000);
  myservo.writeMicroseconds(RED);
  delay(1000);
  myservo.writeMicroseconds(GREEN);
  delay(1000);
  myservo.writeMicroseconds(BLUE);
  delay(1000);
  myservo.writeMicroseconds(GRAY);
  delay(1000);
  myservo.writeMicroseconds(YELLOW);
  delay(1000);
  myservo.writeMicroseconds(MAGENTA);
  delay(1000);
  myservo.writeMicroseconds(CYAN);
  while(1){}
}

