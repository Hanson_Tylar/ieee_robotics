#define RETRACT 1000
#define EXTEND 2000

#include <Wire.h>
#include <Servo.h>
Servo myservo;
void setup() {
  // put your setup code here, to run once:
  pinMode(7, INPUT_PULLUP); //actuator button
  pinMode(6, INPUT_PULLUP); //magnet button
  pinMode(8, OUTPUT);
  myservo.attach(9);
}

void loop() {
  if (digitalRead(6) == 0) {
//    myservo.writeMicroseconds(EXTEND);
    digitalWrite(8, HIGH);
  }else{
//    myservo.writeMicroseconds(RETRACT);
    digitalWrite(8, LOW);
  }

}
